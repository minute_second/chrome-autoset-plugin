$(function () {
    var haveInited =  localStorage.getItem("init");
    if(haveInited != "1"){
        var timestamp = Date.parse(new Date());
        addPanel("",timestamp);
    }else{
        // 已经有数据了则 初始化
       var groupNames = JSON.parse(localStorage.getItem("groupNames"));
       for(let i=0;i<groupNames.length;i++){
           var groupName = JSON.parse(groupNames[i]);
           addPanel(groupName.name,groupName.key);
           // 添加tr元素
           var groupItems = JSON.parse(localStorage.getItem(groupName.key));
           if(groupItems==undefined){
               continue;
           }
           for(let j=0;j<groupItems.length;j++){
               let item = JSON.parse(groupItems[j]);
               let item2 = JSON.parse(groupItems[++j]);
               addTr2(j,groupName.key,item.desc,item.key,item.type,item.value,item2.desc,item2.key,item2.type,item2.value);
           }
        }
    }
    rebind();

})
function getGroupName(){
    var groupNames = JSON.parse(localStorage.getItem("groupNames"));
    return groupNames;
}
function rebind(){
    $(".addTr-add").unbind("click").click(function (e) {
       // $(this).siblings("table").append(newTr);
        var trSize = $(this).siblings("table").find("tr").size();
        var timestamp = $(this).parent().parent().attr("delkey");
        addTr2(trSize+1,timestamp,'','','','','','','','');
    });
    $(".addTr-remove").unbind("click").click(function (e) {
        var size = $(this).siblings("table").find("tr").size();
        if(size ==1){
            return;
        }
        $(this).siblings("table").find("tr:last").remove();
    });
    $(".panel-add").unbind("click").click(function (e) {
        addPanel("","1");
    });
    $(".panel-save").unbind("click").click(function (e) {
        var timeKey = $(this).attr("key");
        saveGroup(timeKey);
    });
    $(".panel-remove").unbind("click").click(function (e) {
        var timeKey = $(this).attr("key");
        removeGroup(timeKey);
    });
}


function execSetJs(pInput,sInput,type) {
    if(sInput==""){
        return;
    }
    var sValue= parseRegExp(sInput);
    if(type=="text"){
       chrome.tabs.getSelected(null, function (tab) {
         chrome.tabs.executeScript(null,{"code":"javascript:document.querySelector('"+pInput+"').value='"+sValue+"'"});
        });
       return;
    }
    if(type=="vue_text"){
       chrome.tabs.getSelected(null, function (tab) {
         chrome.tabs.executeScript(null,{"code":"javascript:document.querySelector('"+pInput+"').value='"+sValue+"'"});
        });
       return;
    }
    if(type=="input"){
        chrome.tabs.getSelected(null, function (tab) {
            chrome.tabs.executeScript(null,{"code":"javascript:document.querySelector('"+pInput+"').value='"+sValue+"'"});
        });
        return;
    }
    if(type=="select"){
        chrome.tabs.getSelected(null, function (tab) {
            chrome.tabs.executeScript(null,{"code":"javascript:document.querySelector('"+pInput+"').value='"+sValue+"'"});
        });
        return;
    }
    if(type=="js"){
        chrome.tabs.getSelected(null, function (tab) {
            chrome.tabs.executeScript(null,{"code":"javascript:"+sValue});
        });
        return;
    }
    if(type=="jquery"){
        chrome.tabs.getSelected(null, function (tab) {
            chrome.tabs.executeScript(null,{file: "js/jquery-1.11.1.min.js",allFrames: true,runAt: "document_idle"});
            chrome.tabs.executeScript(null,{"code":"javascript:"+sValue+";"});
        });
        return;
    }
    chrome.tabs.getSelected(null, function (tab) {
        chrome.tabs.executeScript(null,{"code":"javascript:document.querySelector('"+pInput+"').value='"+sValue+"'"});
    });
}

function execSetupOperaGroup(operaGroupName){
    var groupNames = JSON.parse(localStorage.getItem("groupNames"));
    for(let i=0;i<groupNames.length;i++){
        groupName = JSON.parse(groupNames[i]);
        if(groupName.name==operaGroupName){
            var groupItems = JSON.parse(localStorage.getItem(groupName.key));
            if(groupItems==undefined || groupItems.length==0){
                continue;
            }
            for(let j=0;j<groupItems.length;j++){
                var item = JSON.parse(groupItems[j]);
                execSetJs(item.key,item.value,item.type);
            }
        }
    }
}


var newTr=`
            <tr >
                <td style="">
                    <input  findKey="dataTr" operaType="key" type="text" class="form-control" placeholder="描述" aria-describedby="basic-addon1"  >
                </td>
                <td style="">
                    <input  findKey="dataTr" operaType="key" type="text" class="form-control" placeholder="定位符" aria-describedby="basic-addon1"  >
                </td>
                 <td style="">
                    <input  findKey="dataTr" operaType="value" type="text" class="form-control" placeholder="类型" aria-describedby="basic-addon1"   >
                </td>
                <td style="">
                    <input  findKey="dataTr" operaType="value" type="text" class="form-control" placeholder="值" aria-describedby="basic-addon1"   >
                </td>
              <td style="">
                    <input  findKey="dataTr" operaType="key" type="text" class="form-control" placeholder="描述" aria-describedby="basic-addon1"  >
                </td>
                                <td style="">
                <input  findKey="dataTr"type="text" operaType="key" class="form-control" placeholder="定位符" aria-describedby="basic-addon1"  >
                </td>
                <td style="">
                <input  findKey="dataTr"type="text" operaType="key" class="form-control" placeholder="类型" aria-describedby="basic-addon1"  >
                </td>
             <td style="">
             <input findKey="dataTr" type="text"  operaType="value" class="form-control" placeholder="值" aria-describedby="basic-addon1"  >
                </td>   
            </tr>
  `;
function removeTr(a){
    $(a).siblings("table").find("tr:last").remove();
}
function removeItemGroupName(key){
    var groupNames = JSON.parse(localStorage.getItem("groupNames"));
    var newGroupName=[];
    for(var i=0;i<groupNames.length;i++){
        var groupName =JSON.parse(groupNames[i]);
        if(groupName["key"]==key){
            continue;
        }
        newGroupName.push(JSON.stringify(groupName));
    }
    localStorage.setItem("groupNames",JSON.stringify(newGroupName));
}
function removeGroup(timestampKey){
   var size = $("div[panel=1]").size();
   if(size==1){
       layer.msg('至少保留一个操作组!', {icon: 2});
       return;
   }
    $("div[delkey="+timestampKey+"]").remove();
    localStorage.removeItem(timestampKey);
    removeItemGroupName(timestampKey);
    layer.msg('成功删除!');
}
function saveGroup(timestampKey){
    localStorage.setItem("init","1");
    var name = $("#"+timestampKey).val();
    if(name==""){
        layer.msg('操作组名称不允许为空!', {icon: 2});
        return;
    }
    var tableEle= $("#"+timestampKey).parent().next().find("table");
    var size = $(tableEle).find("input").size();
    var groupNames = JSON.parse(localStorage.getItem("groupNames"));
    var skip= false;
    if(groupNames ==null){
        groupNames=[];
        var temp={};
        temp.key=timestampKey;
        temp.name=name;
        groupNames.push(JSON.stringify(temp));
        localStorage.setItem("groupNames",JSON.stringify(groupNames));
    }else{
        var tempGroupNames=[];
        var find = false;
        for(let i=0;i<groupNames.length;i++){
            var groupName = JSON.parse(groupNames[i]);
            var temp={};
            //查看是否已经存在
            var gKey = groupName.key;
            var gName = groupName.name;
            if(gName==name && gKey!=timestampKey){
                layer.msg('操作组重复!', {icon: 2});
                return;
            }
            if(gKey==timestampKey){
                find =true;
                var temp2 ={};
                temp2.key=timestampKey;
                temp2.name=name;
                tempGroupNames.push(JSON.stringify(temp2));
                continue;
            }
            temp.key=gKey;
            temp.name=gName;
            tempGroupNames.push(JSON.stringify(temp));
        }
        if(!find){
            var temp2 ={};
            temp2.key=timestampKey;
            temp2.name=name;
            tempGroupNames.push(JSON.stringify(temp2));
        }
        localStorage.setItem("groupNames",JSON.stringify(tempGroupNames));
    }
    if(size==0){
        localStorage.removeItem(timestampKey);
        layer.msg('保存成功!');
        return;
    }
    var groupItems=[];
    for(let i=0;i<size;i++){
        var item={};
        var desc = $(tableEle).find("input").eq(i).val();
        var key = $(tableEle).find("input").eq(++i).val();
        var type = $(tableEle).find("input").eq(++i).val();
        var value = $(tableEle).find("input").eq(++i).val();
        item.desc=desc;
        item.type=type;
        item.key=key;
        item.value=value;
        let itemJson = JSON.stringify(item);
        groupItems.push(itemJson);
    }
    let result = JSON.stringify(groupItems);
    localStorage.setItem(timestampKey,result);
    layer.msg('保存成功!');
    $("input[inputStamp="+timestampKey+"]").each(function () {
        $(this).attr("title",$(this).val());
    });
}


function addPanel(operaGroupName,timestamp) {
    if(timestamp=="1"){
        timestamp= Date.parse(new Date());
    }
    var newPanel = `
<div class="panel panel-success" panel="1" delkey="${timestamp}" >
    <div  class="panel-heading"><span >操作组-命令名</span><input    type="text"  class="form-control" style="display: inline;margin: 2px;width: 200px;height: 26px;" placeholder="命令名" aria-describedby="basic-addon1" id='${timestamp}' value="${operaGroupName}" ><span  class="panel-add label label-success"  style="float: right;border: 1px solid #b7d2ff;border-shadow:#b7d2ff;cursor: pointer"> &nbsp;&nbsp;＋&nbsp;&nbsp;</span><span class="panel-remove label label-warning" key="${timestamp}" style="float: right;border: 1px solid #b7d2ff;border-shadow:#b7d2ff;cursor: pointer"> &nbsp;&nbsp;-&nbsp;&nbsp;</span><span class="panel-save label label-primary"  key="${timestamp}" style="float: right;border: 1px solid #b7d2ff;border-shadow:#b7d2ff;cursor: pointer"> &nbsp;&nbsp;✓&nbsp;&nbsp;</span></div>
    <div class="panel-body">
        <span class="addTr addTr-add label label-success"  style="float: right;border: 1px solid #cad96c;border-shadow:#b7d2ff;cursor: pointer"> &nbsp;&nbsp;＋&nbsp;&nbsp;</span><span class="addTr addTr-remove label label-warning"  style="float: right;border: 1px solid #cad96c;border-shadow:#b7d2ff;cursor: pointer"> &nbsp;&nbsp;-&nbsp;&nbsp;</span>
        <table class="table" class="originTr">
            <tr class="originTr" >
               <td style="">描述</td> <td style="">定位符</td><td style="">类型</td><td style="">设置值</td>
                <td style="">描述</td><td style="">定位符</td><td style="">类型</td><td style="">设置值 </td>
            </tr>
        </table>
    </div>
</div>
    `;
    $("body").append(newPanel);
    rebind();
}

function  addTr2(j,timestamp,desc1,key1,type1,value1,desc2,key2,type2,value2) {
    var newTr2=`
            <tr >
                 <td  style="">
                    <input findKey="dataTr" type="text" operaType="desc" class="form-control" placeholder="描述" aria-describedby="basic-addon1" inputStamp="${timestamp}" key="${j}${timestamp}desc1"   >
                </td>
                <td  style="">
                    <input findKey="dataTr" type="text" operaType="key" class="form-control" placeholder="定位符" aria-describedby="basic-addon1" inputStamp="${timestamp}"  key="${j}${timestamp}key1"    >
                </td>
                 <td style="">
                    <input findKey="dataTr" type="text" operaType="value" class="form-control" placeholder="类型" aria-describedby="basic-addon1"  inputStamp="${timestamp}"  key="${j}${timestamp}type1"   >
                </td>
                <td style="">
                    <input findKey="dataTr" type="text" operaType="value" class="form-control" placeholder="值" aria-describedby="basic-addon1"  inputStamp="${timestamp}" key="${j}${timestamp}value1" >
                </td>
                <td  style="">
                    <input findKey="dataTr" type="text" operaType="key" class="form-control" placeholder="描述" aria-describedby="basic-addon1" inputStamp="${timestamp}" key="${j}${timestamp}desc2"  >
                </td>
                <td style="">
                    <input findKey="dataTr" type="text" operaType="key" class="form-control" placeholder="定位符" aria-describedby="basic-addon1" inputStamp="${timestamp}"  key="${j}${timestamp}key2"  >
                </td>
                   <td style="">
                    <input findKey="dataTr" type="text" operaType="key" class="form-control" placeholder="类型" aria-describedby="basic-addon1" inputStamp="${timestamp}" key="${j}${timestamp}type2" >
                </td>
                 <td style="">
                  <input findKey="dataTr" type="text" operaType="value" class="form-control" placeholder="值" aria-describedby="basic-addon1" inputStamp="${timestamp}"  key="${j}${timestamp}value2" >
                </td>   
            </tr>
  `;
   $("#"+timestamp).parent().next().find("table").append(newTr2);
   $("input[key="+j+timestamp+"desc1]").val(desc1);
   $("input[key="+j+timestamp+"key1]").val(key1);
   $("input[key="+j+timestamp+"type1]").val(type1);
   $("input[key="+j+timestamp+"value1]").val(value1);
   $("input[key="+j+timestamp+"desc2]").val(desc2);
   $("input[key="+j+timestamp+"key2]").val(key2);
   $("input[key="+j+timestamp+"type2]").val(type2);
   $("input[key="+j+timestamp+"value2]").val(value2);
   // title
    $("input[key="+j+timestamp+"desc1]").attr("title",desc1);
    $("input[key="+j+timestamp+"key1]").attr("title",key1);
    $("input[key="+j+timestamp+"type1]").attr("title",type1);
    $("input[key="+j+timestamp+"value1]").attr("title",value1);
    $("input[key="+j+timestamp+"desc2]").attr("title",desc2);
    $("input[key="+j+timestamp+"key2]").attr("title",key2);
    $("input[key="+j+timestamp+"type2]").attr("title",type2);
    $("input[key="+j+timestamp+"value2]").attr("title",value2);
    rebind();
}
function  addTr1(j,timestamp,desc1,key1,type1,value1) {
    var newTr2=`
            <tr >
                 <td  style="">
                    <input findKey="dataTr" type="text" operaType="desc" class="form-control" placeholder="描述" aria-describedby="basic-addon1" inputStamp="${timestamp}" key="${j}${timestamp}desc1"   >
                </td>
                <td  style="">
                    <input findKey="dataTr" type="text" operaType="key" class="form-control" placeholder="定位符" aria-describedby="basic-addon1" inputStamp="${timestamp}"  key="${j}${timestamp}key1"    >
                </td>
                 <td style="">
                    <input findKey="dataTr" type="text" operaType="value" class="form-control" placeholder="类型" aria-describedby="basic-addon1"  inputStamp="${timestamp}"  key="${j}${timestamp}type1"   >
                </td>
                <td style="">
                    <input findKey="dataTr" type="text" operaType="value" class="form-control" placeholder="值" aria-describedby="basic-addon1"  inputStamp="${timestamp}" key="${j}${timestamp}value1" >
                </td>
            </tr>
  `;
    $("#"+timestamp).parent().next().find("table").append(newTr2);
    $("input[key="+j+timestamp+"desc1]").val(desc1);
    $("input[key="+j+timestamp+"key1]").val(key1);
    $("input[key="+j+timestamp+"type1]").val(type1);
    $("input[key="+j+timestamp+"value1]").val(value1);
    // title
    $("input[key="+j+timestamp+"desc1]").attr("title",desc1);
    $("input[key="+j+timestamp+"key1]").attr("title",key1);
    $("input[key="+j+timestamp+"type1]").attr("title",type1);
    $("input[key="+j+timestamp+"value1]").attr("title",value1);
    rebind();
}