/**
 * 主js
 * **/
$(function () {
    $("#setOperaGroup").click(function () {
        window.open(chrome.extension.getURL("back.html"))
    })
    $("#setOpera").click(function () {
        var operaGroupName=$("#operaGroupName").val();
        var select=$("#groupSelect").val();
        var backPage = chrome.extension.getBackgroundPage();
        if(operaGroupName!=""){
            backPage.execSetupOperaGroup(operaGroupName);
            return;
        }else{
            backPage.execSetupOperaGroup(select);
            return;
        }

    });
    $(".opti").click(function (e) {
        $("#groupSelect").val($(this).attr("key"));
    });
    $("#groupSelect").click(function () {
        var operaGroupName=$("#operaGroupName").val();
        var backPage = chrome.extension.getBackgroundPage();
        var a= backPage.getGroupName();
        $(this).html("<option class='form-control'>存在组</option>");
        if(a.length>0){
            for(var i=0;i<a.length;i++){
                var json = JSON.parse(a[i]);
                $(this).append("<option class='opti' key='"+json.name+"'>"+json.name+"</option>");
            }
        }
        $("#groupSelect").unbind("click");
    });

})

