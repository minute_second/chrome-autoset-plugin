/**
 * 根据正则
 *
 * **/
function toArray(originKey){
    var key = originKey.replaceAll("[","").replaceAll("]","");// 去除中括号
    var remain=key;
    var array = [];
    var i=0;
    while(remain.length>0){
        if(remain.startsWith("A(") || remain.startsWith("a(") || remain.startsWith("中(") || remain.startsWith("N(") || remain.startsWith("X(") || remain.startsWith("R(") ){
            var findIndex = remain.indexOf(")");
            array[i]=remain.substring(0,findIndex+1);
            remain = remain.substring(findIndex+1);
            if(remain.startsWith(",")){
                remain = remain.substring(1);
            }
            i++;
            continue;
        }
        var findIndex = remain.indexOf(",");
        if(findIndex!=-1){
            array[i]=remain.substring(0,findIndex);
            remain = remain.substring(findIndex+1);
        }else{
            array.push(remain);
            remain="";
        }
        i++;
    }
    return array;
}

function randomNum(minNum,maxNum){
    minNum = parseInt(minNum);
    maxNum = parseInt(maxNum);
    switch(arguments.length){
        case 1:
            return parseInt(Math.random()*minNum+1,10);
            break;
        case 2:
            return parseInt(Math.random()*(maxNum-minNum+1)+minNum,10);
            break;
        default:
            return 0;
            break;
    }

}
function getRandomText() {
    var words = "";
    eval("var word=" + '"\\u' + (Math.round(Math.random() * 20901) + 19968).toString(16) + '"');
    words += word;
    return word;
}
function getText(n,m){
    var words = "";
    var count = randomNum(n,m);
    for(var i=0;i<count;i++){
        words=words+ getRandomText();
    }
    return words;
}
function getENGLISH(n,m){
    var lookupTimes = randomNum(n,m);
    var ENGLISH=['A','B','C','D','E','F','G','H','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    var words = "";
    for(var i=0;i<lookupTimes;i++){
        words=words+ ENGLISH[ Math.floor(Math.random()*26)];
    }
    return words;
}
function  getX(n) {
    var words = "";
    var XArray=[',','(',')','"'];
    for(i=0;i<n.length;i++){
        var arg = n[i];
        if(XArray[i] ==undefined){
            continue;
        }
        words+=XArray[i];
    }
    return words;
}

function  getN(n,m) {
    var words = "";
    var lookupTimes = randomNum(n,m);
    for(var i=0;i<lookupTimes;i++){
        words+=Math.floor(Math.random(10)*10);
    }
    return words;
}
function  getWRang(list,n,m) {
    var words = "";
    var keys = list.split("");
    var lookupTimes = randomNum(n,m);
    for(var i=0;i<lookupTimes;i++){
        var randomKey = keys[Math.floor(Math.random(10)*keys.length)];
        words+=randomKey;
    }
    return words;
}

function  getEnglish(n,m) {
    var lookupTimes = randomNum(n,m);
    var english=['a','b','c','d','e','f','g','h','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    var words = "";
    for(var i=0;i<lookupTimes;i++){
        words=words+ english[ Math.floor(Math.random()*26)];
    }
    return words;
}

function parseRegExp(regExp){
    var result = "";
    if(regExp.toString().length<8)return regExp;
    var regArray = regExp.substr(6);
    if(!regArray.endsWith("]")||!regArray.startsWith("[")){
        return regExp;
    }
    // 转数组
    var realArray = toArray(regArray);
    for(var i=0;i<realArray.length;i++){
        // 开始分析
        var key = realArray[i];
        if(key.startsWith("中(")&&key.endsWith(")")){
            // 获取个数
            var nm= key.replaceAll("中(","").replaceAll(")","").split(",");
            result+= getText(nm[0],nm[1]);
            continue;
        }
        if(key.startsWith("A(")&&key.endsWith(")")){
            // 获取个数
            var nm= key.replaceAll("A(","").replaceAll(")","").split(",");
            result+= getENGLISH(nm[0],nm[1]);
            continue;
        }
        if(key.startsWith("a(")&&key.endsWith(")")){
            // 获取个数
            var nm= key.replaceAll("a(","").replaceAll(")","").split(",");
            result+= getEnglish(nm[0],nm[1]);
            continue;
        }
        if(key.startsWith("N(")&&key.endsWith(")")){
            // 获取个数
            var nm= key.replaceAll("N(","").replaceAll(")","").split(",");
            result+= getN(nm[0],nm[1]);
            continue;
        }
        if(key.startsWith("X(")&&key.endsWith(")")){
            // 获取个数
            var nm= key.replaceAll("X(","").replaceAll(")","").split(",");
            result+= getX(nm);
            continue;
        }
        if(key.startsWith("R(")&&key.endsWith(")")){
            // 获取个数
            var nm= key.replaceAll("R(","").replaceAll(")","").split(",");
            result+= getWRang(nm[0],nm[1],nm[2]);
            continue;
        }
        result+=key;
    }
    return result;
}